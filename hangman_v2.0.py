# -*- coding: utf-8 -*-

import tkinter as tk
from tkinter import messagebox, PhotoImage
import random
import tkinter.font as tkFont

class Hangman():
    def __init__(self, window):
        self.custom_font_18 = tkFont.Font(family="Helvetica", size=18, weight="bold")
        self.custom_font_14 = tkFont.Font(family="Helvetica", size=14, weight="bold")
        self.window = window
        self.gallow = [tk.PhotoImage(file=f"E:\ZaRaczke\EGZAMINY\Game_wisielec_tk\png\h{i}.png") for i in range(8)]
        self.word_list = ['PARASOLKA', 'POMIDOREK', 'SKAKANKA', 'PRZEDSZKOLE', 'HULAJNOGA']
        self.max_mistake = 7
        self.word = random.choice(self.word_list)
        self.space = ['_'] * len(self.word)
        self.stop = False
        self.continiue = False
        self.added_letter = False
        self.used_letters = set()
        self.counter_letter_in_space = 0
        self.counter_letter_in_word = 0
        self.mistake = 0
        self.letter = "0"

        self.label_intro = tk.Label(window, text='GRA WISIELEC', font=self.custom_font_18)
        self.label_intro.pack(padx=10, pady=10)

        self.label_letter = tk.Label(window, text='Podaj literę:', font = self.custom_font_18)
        self.label_letter.pack(padx=10, pady=10)

        self.input_entry = tk.Entry(window, font = self.custom_font_14, width=5)
        self.input_entry.pack(padx=10, pady=10)

        self.input_button = tk.Button(window, text="Sprawdź", command=self.play, font = self.custom_font_14)
        self.input_button.pack(padx=10, pady=10)

        self.label_answer = tk.Label(window, text="Hasło", font=self.custom_font_14)
        self.label_answer.pack(padx=10, pady=10)

        self.label_space = tk.Label(window, text=' '.join(self.space), font = self.custom_font_18)
        self.label_space.pack(padx=10, pady=10)

        self.label_mistake = tk.Label(window, text=f"Masz jeszcze: {self.max_mistake-self.mistake} prób", font = self.custom_font_14)
        self.label_mistake.pack(padx=10, pady=10)

        self.label_graf = tk.Label(window, image=f"{self.gallow[self.mistake]}")
        self.label_graf.image = self.gallow[self.mistake]
        self.label_graf.pack()

    def add_letter(self):
        letter_user = self.input_entry.get()
        self.letter = letter_user.upper()
        if self.letter in self.used_letters:
            messagebox.showinfo("Informacja", "Ta litera została już podana.")
            self.added_letter = False
        elif len(self.letter) != 1 or not self.letter.isalpha():
            messagebox.showinfo("Informacja", "To  nie jest litera")
            self.added_letter = False
        else:
            self.used_letters.add(self.letter)
            self. added_letter = True
        return self.added_letter

    def check_letter(self):
        for i in range(len(self.word)):
            if self.word[i] == self.letter:
               self.space[i] = self.letter

    def count_letter_in_word(self):
        for i in range(len(self.word)):
            if self.word[i] == self.letter and self.added_letter == True:
               self.counter_letter_in_word = self.counter_letter_in_word + 1
            else:
                self.counter_letter_in_word = self.counter_letter_in_word
    def count_letter_in_space(self):
        for i in range(len(self.word)):
             if self.space[i] != '_':
                self.counter_letter_in_space = self.counter_letter_in_space + 1
        #print(self.counter_letter_in_space)
    def set_stop(self):
        if self.counter_letter_in_space == len(self.word):
            self.stop = True
        else:
            self.stop = False
    def count_mistake(self):
       if self.counter_letter_in_word == 0 and self.added_letter:
            self.mistake = self.mistake + 1
            messagebox.showinfo("Informacja", "Ta litera nie występuje")
       else:
           self.mistake = self.mistake
       self.config_widget()

    def config_widget(self):
        self.input_entry.delete(0, tk.END)
        self.label_mistake.config(text=f"Masz jeszcze: {7 - self.mistake} prób")
        self.label_graf.config(image=f"{self.gallow[self.mistake]}")
        self.label_graf.image = self.gallow[self.mistake]
        self.label_space.config(text=' '.join(self.space))
    def ask_question(self):
        answer = messagebox.askquestion("Pytanie", "Czy chcesz kontynuować?")
        if answer == "yes":
            self.continiue = True
        else:
            self.continiue = False
    def restart_game(self):
        self.word = random.choice(self.word_list)
        self.space = ['_'] * len(self.word)
        self.used_letters.clear()
        self.mistake = 0
        self.config_widget()

    def close_window(self):
        self.window.destroy()

    def play_again(self):
        self.ask_question()
        if self.continiue:
            self.restart_game()
        else:
            self.close_window()
    def play(self):
        if    not self.stop  and self.mistake < self.max_mistake:
              self.add_letter()
              self.check_letter()
              self.count_letter_in_word()
              self.count_letter_in_space()
              self.set_stop()
              self.count_mistake()
              self.counter_letter_in_word = 0
              self.counter_letter_in_space = 0

              if self.stop and self.mistake < self.max_mistake:
                  messagebox.showinfo("WYGRANA", "Gratulacje! Odgadłeś słowo!")
                  self.ask_question()
                  self.play_again()
              elif self.mistake == self.max_mistake:
                  messagebox.showinfo("PRZEGRANA", f"Koniec gry! Słowo to: {self.word}")
                  self.play_again()

def main():
    window = tk.Tk()
    window.geometry('700x700')
    window.title("WISIELEC")
    Hangman(window)
    window.mainloop()

if __name__ == "__main__":
    main()



