# -*- coding: utf-8 -*-

import tkinter as tk
from tkinter import messagebox, PhotoImage
import random
import tkinter.font as tkFont

class Hangman_Logic():
    def __init__(self):
        self.word_list = ['PARASOLKA', 'POMIDOREK', 'SKAKANKA', 'PRZEDSZKOLE', 'HULAJNOGA']
        self.max_mistake = 2
        self.word = random.choice(self.word_list)
        self.space = ['_'] * len(self.word)
        self.stop = False
        self.added_letter = False
        self.used_letters = set()
        self.counter_letter_in_space = 0
        self.counter_letter_in_word = 0
        self.mistake = 0
        self.letter = "0"

    def check_letter(self):
        for i in range(len(self.word)):
            if self.word[i] == self.letter:
                self.space[i] = self.letter

    def add_letter(self, letter_user, message_4, message_5):
        self.letter = letter_user().upper()
        if self.letter in self.used_letters:
            message_4()            # tu wywołanie funkcji, nie w def
            self.added_letter = False
        elif len(self.letter) != 1 or self.letter.isalpha() == False:
            message_5()
            self.added_letter = False
        else:
            self.used_letters.add(self.letter)
            self.added_letter = True
        return self.added_letter

    def count_letter_in_word(self):
        for i in range(len(self.word)):
            if self.word[i] == self.letter and self.added_letter == True:
                self.counter_letter_in_word = self.counter_letter_in_word + 1
            else:
                self.counter_letter_in_word = self.counter_letter_in_word
        return self.counter_letter_in_word
    def count_letter_in_space(self):
        for i in range(len(self.word)):
            if self.space[i] != '_':
                self.counter_letter_in_space = self.counter_letter_in_space + 1
        return self.counter_letter_in_space

    def set_stop(self):
        if self.counter_letter_in_space == len(self.word):
            self.stop = True
        else:
            self.stop = False

    def count_mistake(self, message_6, config_widget):
        if self.counter_letter_in_word == 0 and self.added_letter:
            self.mistake = self.mistake + 1
            message_6()
            config_widget()
        else:
            self.mistake = self.mistake
            config_widget()
        return self.mistake
class Hangman_UI():
    def __init__(self, window, play, logic):
        self.window = window
        self.play = play
        self.logic = logic

        self.custom_font_18 = tkFont.Font(family="Helvetica", size=18, weight="bold")
        self.custom_font_14 = tkFont.Font(family="Helvetica", size=14, weight="bold")

        self.gallow = [tk.PhotoImage(file=f"E:\ZaRaczke\EGZAMINY\Game_wisielec_tk\png\h{i}.png") for i in range(8)]

        self.label_intro = tk.Label(window, text='GRA WISIELEC', font=self.custom_font_18)
        self.label_intro.pack(padx=10, pady=10)

        self.label_letter = tk.Label(window, text='Podaj literę:', font=self.custom_font_18)
        self.label_letter.pack(padx=10, pady=10)

        self.input_entry = tk.Entry(window, font = self.custom_font_14, width=5)
        self.input_entry.pack(padx=10, pady=10)

        self.input_button = tk.Button(window, text="Sprawdź", command=self.trigger_play, font=self.custom_font_14)
        self.input_button.pack(padx=10, pady=10)

        self.label_answer = tk.Label(window, text="Hasło", font=self.custom_font_14)
        self.label_answer.pack(padx=10, pady=10)

        self.label_space = tk.Label(self.window, text=' '.join(self.logic.space), font=self.custom_font_18)
        self.label_space.pack(padx=10, pady=10)

        self.label_mistake = tk.Label(window, text=f"Masz jeszcze: {7 - self.logic.mistake} prób", font=self.custom_font_14)
        self.label_mistake.pack(padx=10, pady=10)

        self.label_graf = tk.Label(window, image=f"{self.gallow[self.logic.mistake]}")
        self.label_graf.image = self.gallow[self.logic.mistake]
        self.label_graf.pack()

    def trigger_play(self):
        self.play()
    def entry_get(self):
        return self.input_entry.get()
    def message_1(self):
        return messagebox.askquestion("Pytanie", "Czy chcesz kontynuować?") #bez return nie działa

    def message_2(self):
        messagebox.showinfo("WYGRANA", "Gratulacje! Odgadłeś słowo!")

    def message_3(self):
        messagebox.showinfo("PRZEGRANA", f" Koniec gry! ")

    def message_4(self):
        messagebox.showinfo("Informacja", "Ta litera została już podana.")

    def message_5(self):
        messagebox.showinfo("Informacja", "To  nie jest litera")

    def message_6(self):
        messagebox.showinfo("Informacja", "Ta litera nie występuje")

    def config_widget(self):
        self.input_entry.delete(0, tk.END)
        self.label_mistake.config(text=f"Masz jeszcze: {7 - self.logic.mistake} prób")
        self.label_graf.config(image=f"{self.gallow[self.logic.mistake]}")
        self.label_graf.image = self.gallow[self.logic.mistake]
        self.label_space.config(text=' '.join(self.logic.space))

class Hangman_Play():
    def __init__(self, window):
            self.window = window
            self.continiue = False
            self.hangman_logic = Hangman_Logic()
            self.hangman_ui = Hangman_UI(window, self.play, self.hangman_logic)

    def restart_game(self):
            self.hangman_logic.word = random.choice(self.hangman_logic.word_list)
            self.hangman_logic.space = ['_'] * len(self.hangman_logic.word)
            self.hangman_logic.used_letters.clear()
            self.hangman_logic.mistake = 0
            self.hangman_ui.config_widget()
    def close_window(self):
            self.window.destroy()

    def play_again(self):
            self.ask_question()
            if self.continiue:
                self.restart_game()
            else:
                self.close_window()
    def ask_question(self):
        answer = self.hangman_ui.message_1()
        if answer == "yes":
            self.continiue = True
        else:
            self.continiue = False

    def play(self):
        if not self.hangman_logic.stop and self.hangman_logic.mistake < self.hangman_logic.max_mistake:
              self.hangman_logic.add_letter(self.hangman_ui.entry_get, self.hangman_ui.message_4, self.hangman_ui.message_5)

              self.hangman_logic.check_letter()
              self.hangman_logic.count_letter_in_word()

              self.hangman_logic.count_letter_in_space()
              self.hangman_logic.set_stop()

              self.hangman_logic.count_mistake(self.hangman_ui.message_6, self.hangman_ui.config_widget)
              # print(self.hangman_logic.letter)
              # print(self.hangman_logic.mistake)
              # print(self.hangman_logic.counter_letter_in_word)
              # print(self.hangman_logic.added_letter)
              print(self.continiue)
              self.hangman_logic.counter_letter_in_word = 0
              self.hangman_logic.counter_letter_in_space = 0

              if self.hangman_logic.stop and self.hangman_logic.mistake < self.hangman_logic.max_mistake:
                  self.hangman_ui.message_2()
                  self.ask_question()
                  self.play_again()
              elif self.hangman_logic.mistake == self.hangman_logic.max_mistake:
                  self.hangman_ui.message_3()
                  self.play_again()

def main():
       window = tk.Tk()
       window.geometry('700x700')
       window.title("WISIELEC")
       object = Hangman_Play(window)
       # print(object.hangman_logic.mistake)
       # print(object.hangman_logic.word)
       # print(object.hangman_ui.entry_get())
       # print(object.hangman_logic.counter_letter_in_word)
       # print(object.hangman_logic.added_letter)
       window.mainloop()

if __name__ == "__main__":
    main()



